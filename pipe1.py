import matplotlib.pyplot as plt
import numpy as np
import os
import pseg
import pred
import torchvision
from PIL import Image

"""
pipe.py

- prende in input l'immagine di un vassoio contenente diverse piante in vasetto
- individua le piante tramite una segmentazione basata sul colore
- segmenta singolarmente le piante utilizzando SegNet
- ricostruisce l'immagine del vassoio segmentato
"""

def pipeline(path):
    img = Image.open(path)

    # inizializza sfondo nero dove verranno inserite le piante segmentate
    img_seg = np.zeros(np.shape(img)[:2])

    if img.mode == 'RGBA':
        data_conv = np.array(img)
        r, g, b, a = np.rollaxis(data_conv, axis=-1)
        data_conv = np.dstack([r, g, b])
        img = Image.fromarray(data_conv)

    bbox = pseg.plantsdetection(path)

    # elabora una pianta alla volta con SegNet
    for i in range(len(bbox)):
        [x, y, w, h] = bbox[i]
        img_crop = img.crop((x, y, x+w, y+h))

        # key indica il modello che si vuole usare per la segmentazione
        img_net = pred.cnnsegmentation(img_crop, thresh=0.50, key='m14')

        # le immagini vengono resizate a 224x224 per ottenere risultati migliori
        # la rete è stata trainata e finetunata con quelle dimensioni
        # ma funziona anche senza effettuare il resize
        img_pil = Image.fromarray(img_net*255)
        trans_resize = torchvision.transforms.Resize((h, w))
        img_net = trans_resize(img_pil)
        img_net = np.array(img_net)

        img_net[img_net > np.max(img_net) / 2] = 1
        img_net[img_net != 1] = 0

        """
        fig = plt.figure()
        ax1 = fig.add_subplot(121)
        ax2 = fig.add_subplot(122)
        ax1.imshow(img_crop)
        ax2.imshow(img_net, 'gray')
        plt.ion()
        plt.pause(2)
        plt.show()
        plt.close()
        """

        img_seg[y:(y+h), x:(x+w)] = img_net

    """
    fig = plt.figure()
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)
    ax1.imshow(img)
    ax2.imshow(img_seg, 'gray')
    plt.show()
    """

    return img_seg


def main():
    path = './datasets/Plant_Phenotyping_Datasets/Tray/.../...'
    pipeline(path)

    # codice per elaborare una folder e salvare i risultati
    """
    dir = './datasets/Plant_Phenotyping_Datasets/Tray/...'
    output_dir = './output'

    images = [os.path.join(dir, name) for name in os.listdir(dir) if
              os.path.isfile(os.path.join(dir, name)) & name.endswith('_rgb.png')]

    for i in images:
        filename = os.path.join(output_dir, os.path.basename(i))
        print(filename)

        file = pipeline(i)
        file = Image.fromarray(file*255)
        file = file.convert('1')
        file.save(filename, bit=1)
    """


main()
