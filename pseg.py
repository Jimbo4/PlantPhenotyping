import cv2
import matplotlib.pyplot as plt
import numpy as np
import os

"""
pseg.py

- prende in input l'immagine di un vassoio contenente diverse piante in vasetto
- individua e segmenta le piante tramite un semplice approccio basato sul colore
"""


def union(a, b):
    """
    - unisce bounding box foglie per formare bounding box finale pianta
    """

    x = min(a[0], b[0])
    y = min(a[1], b[1])
    w = max(a[0] + a[2], b[0] + b[2]) - x
    h = max(a[1] + a[3], b[1] + b[3]) - y

    return x, y, w, h


def intersection(a, b):
    """
    - gestisce sovrapposizione buonding box (con un margine di overlap)
    """

    overlap = 42
    x = max(a[0], b[0])
    y = max(a[1], b[1])
    w = min(a[0] + a[2] + overlap, b[0] + b[2] + overlap) - x
    h = min(a[1] + a[3] + overlap, b[1] + b[3] + overlap) - y

    if w < 0 or h < 0:
        return False
    else:
        return True


def doublecheck(a, b):
    """
    - determina quali bounding box debbano essere unite per formare un'unica pianta e quali devono essere separate
    in quanto appartengono a piante diverse
    """

    c = union(a, b)
    c_area = c[2] * c[3]

    if c_area > 300000 or c[2] > (9 / 5) * c[3]:
        return False
    else:
        return True


def combineboxes(bbox):
    """
    - utilizza unione, intersezione e controlli per gestire le bonding box individuate
    """

    flag = True
    blacklist = []
    bbox_new = []

    while flag:
        for i in range(len(bbox)):
            for j in range(i + 1, len(bbox)):
                if not (bbox[i] in blacklist) and not (bbox[i] in blacklist):
                    if intersection(bbox[i], bbox[j]) and doublecheck(bbox[i], bbox[j]):
                        bbox_new.append(union(bbox[i], bbox[j]))
                        blacklist.append(bbox[i])
                        blacklist.append(bbox[j])
            if not (bbox[i] in blacklist):
                bbox_new.append(bbox[i])
        if bbox == bbox_new:
            flag = False
        else:
            bbox = bbox_new
            bbox_new = []
            blacklist = []

    return np.array(bbox_new).astype('int')


def plantsdetection(path):
    img = cv2.imread(path, cv2.IMREAD_COLOR)
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    # utilizzata per visualizzare risultati
    """
    img_clone = img.copy()
    """

    # altri valori valutati:
    # [44,54,63]    [71,255,255]
    # [34, 104, 63] [71, 255, 255]
    # [35, 134, 60] [71, 255, 255]
    green_lower = np.array([35, 134, 60])
    green_upper = np.array([71, 255, 255])
    mask = cv2.inRange(hsv, green_lower, green_upper)

    """
    fig = plt.figure()
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)
    ax1.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    ax2.imshow(mask, 'gray')
    plt.show()
    """

    nb_components, output, stats, centroids = cv2.connectedComponentsWithStats(mask, connectivity=8)
    nb_components = nb_components - 1

    # EXTRA
    # pulizia dei risultati
    """
    sizes = stats[1:, -1]
    min_size = 10
    img2 = np.zeros(output.shape)
    for i in range(0, nb_components):
        if sizes[i] >= min_size:
            img2[output == i + 1] = 255
    img2[0:200, 0:np.shape(img2)[1]] = 0

    fig = plt.figure()
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)
    ax1.imshow(mask, 'gray')
    ax2.imshow(img2, 'gray')
    plt.show()
    """

    # individua bounding box parziali
    bbox = []
    for i in range(1, nb_components):
        [x, y, w, h] = stats[i][:-1]
        if w > 10 and h > 10:
            """
            cv2.rectangle(img_clone, (x, y), (x + w, y + h), (0, 0, 255), 10)
            """
            bbox.append((x, y, w, h))

    bbox_new = combineboxes(bbox)
    bbox_new = sorted(bbox_new, key=lambda tup: tup[0])

    # allarga bounding box individuate del 5% e corregge errori di out of bounds
    # questo ingrandimento del 5% è stato fatto per essere conformi alle indicazioni del paper descrivente il dataset
    for i in range(len(bbox_new)):
        [x, y, w, h] = bbox_new[i]

        wdelta = w * 0.05
        hdelta = h * 0.05

        xen = int(round(x - (wdelta / 2)))
        yen = int(round(y - (hdelta / 2)))
        wen = int(round(w + wdelta))
        hen = int(round(h + hdelta))

        if xen + wen > img.shape[1]:
            wen = img.shape[1] - xen

        if yen + hen > img.shape[0]:
            hen = img.shape[0] - yen

        bbox_new[i][0] = xen
        bbox_new[i][1] = yen
        bbox_new[i][2] = wen
        bbox_new[i][3] = hen

        """
        print(bbox_new[i])
        [x, y, w, h] = bbox_new[i]
        cv2.rectangle(img_clone, (x, y), (x + w, y + h), (0, 255, 0), 10)
        """

    """
    plt.imshow(cv2.cvtColor(img_clone, cv2.COLOR_BGR2RGB))
    plt.show()
    """

    return bbox_new


def main():
    path = './datasets/Plant_Phenotyping_Datasets/Tray/.../...'
    bbox = plantsdetection(path)

    # codice per elaborare una folder e salvare i risultati
    """
    dir = './datasets/Plant_Phenotyping_Datasets/Tray/...'
    output_dir = './output'

    images = [os.path.join(dir, name) for name in os.listdir(dir) if
              os.path.isfile(os.path.join(dir, name)) & name.endswith('_rgb.png')]

    images.sort()

    for i in images:

        bbox = plantsdetection(i)
        filename = os.path.join(output_dir, os.path.basename(i))

        bbox_form = []
        for n in range(len(bbox)):
            [x, y, w, h] = bbox[n]
            bbox_cell = []

            bbox_cell.append(x)
            bbox_cell.append(y)

            bbox_cell.append(x)
            bbox_cell.append(y + h)

            bbox_cell.append(x + w)
            bbox_cell.append(y + h)

            bbox_cell.append(x + w)
            bbox_cell.append(y)

            bbox_form.append(bbox_cell)

        # x_show = []
        # y_show = []
        # for l in range(len(bbox_form)):
        #         x_show.append(bbox_form[l][0])
        #         y_show.append(bbox_form[l][1])
        # 
        # img_show = cv2.imread(i, cv2.IMREAD_COLOR)
        # plt.figure()
        # plt.imshow(cv2.cvtColor(img_show, cv2.COLOR_BGR2RGB))
        # plt.scatter(x_show, y_show, c='r')
        # plt.show()

        # np.savetxt(filename + '.csv', bbox_sorted, delimiter=',', fmt='%d')
    """


# main()
