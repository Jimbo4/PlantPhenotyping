import matplotlib.pyplot as plt
import numpy as np
import torch
import torchvision
from PIL import Image
from segnet import SegNet
from torch.autograd import Variable

"""
pred.py

- elabora un'immagine con SegNet
"""


def cnnsegmentation(img, thresh, key):
    model_keys = {'m0': './model/m0e50plant_converted.pth',
                  'm14': './model/m14e27plant_finetuned.pth',
                  'm15': './model/m15e30leaf_finetuned.pth'}

    model = SegNet(3, 2)
    model.load_from_filename(model_keys[key])
    model = model.cuda()
    model.eval()

    trans_resize = torchvision.transforms.Resize((224, 224))
    img = trans_resize(img)

    input = torchvision.transforms.ToTensor()(img).unsqueeze(0)
    input = Variable(input, volatile=True).cuda()

    output = model(input)

    softmax = torch.nn.Softmax(dim=1)
    output_soft = softmax(output)

    output_soft = output_soft.data.cpu().numpy()
    background = output_soft[0][0]
    foreground = output_soft[0][1]

    img_seg = np.copy(foreground)
    img_seg[img_seg > thresh] = 1
    img_seg[img_seg != 1] = 0

    """
    fig = plt.figure()
    ax1 = fig.add_subplot(221)
    ax2 = fig.add_subplot(222)
    ax3 = fig.add_subplot(223)
    ax4 = fig.add_subplot(224)
    ax1.imshow(img)
    ax2.imshow(background, 'gray')
    ax3.imshow(foreground, 'gray')
    ax4.imshow(img_seg, 'gray')
    plt.ion()
    plt.pause(1)
    plt.show()
    plt.close()
    """

    return img_seg


def main():
    img = Image.open('./datasets/CVPPP2017_LSC_training/training/.../...')

    if img.mode == 'RGBA':
        data_conv = np.array(img)
        r, g, b, a = np.rollaxis(data_conv, axis=-1)
        data_conv = np.dstack([r, g, b])
        img = Image.fromarray(data_conv)

    cnnsegmentation(img, thresh=0.50, key='m14')


# main()
