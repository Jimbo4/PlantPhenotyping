import cv2
import matplotlib.pyplot as plt
import numpy as np
import os
from skimage import color
from skimage import io
from skimage.feature import corner_peaks, peak_local_max
from skimage.segmentation import slic, mark_boundaries
from skimage.util import img_as_float

"""
slic.py

- implementazione del metodo Nottingham basato su superpixel e watershed descritto in:
    http://eprints.nottingham.ac.uk/34197/1/MVAP-D-15-00134_Revised_manuscript.pdf
- utilizza lo spazio colore lab
- calcola i superpixel e il valore di verde medio per ogni superpixel
- segmenta basandosi sui superpixel
- calcola mappa di distanza e massimi locali
- utilizza i massimi locali come centroidi delle foglie
- applica watershed per indicare con label diverso ogni foglia
"""


def superpixel(path, a_thres=-20):
    img = img_as_float(io.imread(path))

    if img.shape[2] == 4:
        img = img[:, :, :3]

    img_lab = color.rgb2lab(img)
    plant_mask = np.zeros(img.shape[:2], dtype="uint8")

    # calcola superpixel
    segments_n = 2000
    segments = slic(img, n_segments=segments_n, sigma=1, convert2lab=True)

    """
    plt.imshow(mark_boundaries(img, segments))
    plt.show()
    """

    l, a, b = cv2.split(img_lab)
    segments_ids = np.unique(segments)

    # calcola valore A medio per ogni superpixel
    for v in segments_ids:
        mask = np.ones(img.shape[:2])
        mask[segments == v] = 0
        a_mask = np.ma.masked_array(a, mask=mask)
        a_mean = a_mask.mean()

        # Per le piante della categoria ARA2012 il valore migliore di a_thres è -25
        # Per le piante della categoria ARA2013 il valore migliore di a_thres è -15
        if a_mean < a_thres:
            plant_mask[segments == v] = 255

    """
    plt.imshow(plant_mask, 'gray')
    plt.show()
    """

    # segmenta pianta e rimuove background
    img_seg = cv2.bitwise_and(img, img, mask=plant_mask)

    """
    plt.imshow(img_seg)
    plt.show()
    """

    # calcola mappa distanza e massimi locali come centroidi foglie
    dist_transform = cv2.distanceTransform(plant_mask, cv2.DIST_L2, 5)
    local_max = corner_peaks(dist_transform, min_distance=15, threshold_rel=0.4, indices=True)

    """
    x_show = []
    y_show = []
    for i in range(len(local_max)):
        x_show.append(local_max[i][1])
        y_show.append(local_max[i][0])
    fig = plt.figure()
    ax1 = fig.add_subplot(131)
    ax2 = fig.add_subplot(132)
    ax3 = fig.add_subplot(133)
    ax1.imshow(img_seg)
    ax2.imshow(dist_transform)
    ax3.imshow(img)
    ax3.scatter(x_show, y_show, c='r')
    plt.show()
    """

    img_seed = np.zeros(img.shape[:2])

    # crea seeds per effettuare watershed
    fprint = 3
    for seed in local_max:
        x = seed[1]
        y = seed[0]
        img_seed[y-fprint:y+fprint, x-fprint:x+fprint] = 255

    img_seed = np.array(img_seed, dtype=np.uint8)
    img_hole = cv2.subtract(plant_mask, img_seed)

    _, markers = cv2.connectedComponents(img_seed)
    markers = markers + 1
    markers[img_hole == 255] = 0
    # watershed agisce su = 0

    img_seg = np.array(img_seg*255, dtype=np.uint8)
    markers = cv2.watershed(img_seg, markers)

    # rimozione bordi creati da watershed
    h = markers.shape[0]
    w = markers.shape[1]
    markers[0, :] = 1
    markers[:, 0] = 1
    markers[:, w-1] = 1
    markers[h-1, :] = 1
    img_seg[markers == -1] = [255, 0, 0]
    for i in range(h):
        for j in range(w):
            if markers[i, j] == -1:
                neighborhood = []
                neighborhood.append(markers[i-1, j-1])
                neighborhood.append(markers[i-1, j])
                neighborhood.append(markers[i-1, j+1])
                neighborhood.append(markers[i, j+1])
                neighborhood.append(markers[i+1, j+1])
                neighborhood.append(markers[i+1, j])
                neighborhood.append(markers[i+1, j-1])
                neighborhood.append(markers[i, j-1])

                markers[i, j] = max(set(neighborhood), key=neighborhood.count)

    # TODO: correzione labels
    cmap = plt.cm.get_cmap('jet')
    norm = plt.Normalize(vmin=markers.min(), vmax=markers.max())
    img_cmap = cmap(norm(markers))

    """
    fig = plt.figure()
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)
    ax1.imshow(img_cmap)
    ax2.imshow(img_seg)
    plt.show()
    """

    return img_cmap


def main():
    path = './datasets/...'
    superpixel(path, a_thres=-15)
    # Per le piante della categoria ARA2012 il valore migliore di a_thres è -25
    # Per le piante della categoria ARA2013 il valore migliore di a_thres è -15

    # codice per elaborare una folder e salvare i risultati
    """
    dir = './datasets/...'
    output_dir = './output'

    images = [os.path.join(dir, name) for name in os.listdir(dir) if
              os.path.isfile(os.path.join(dir, name)) & name.endswith('_rgb.png')]

    for i in images:
        file = superpixel(i)
        filename = os.path.join(output_dir, os.path.basename(i))
        plt.imsave(filename, file)
    """


main()
