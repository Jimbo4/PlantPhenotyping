import os
from PIL import Image
from torch.utils.data import Dataset

"""
data.py

- genera il dataset necessario al dataloader di pytorch
- applicando le trasformazioni sia sulle immagini che sulle ground truths
"""


class PlantDataset(Dataset):

    def __init__(self, root_dir_img, root_dir_gt, transform=None):

        self.root_dir_img = root_dir_img
        self.root_dir_gt = root_dir_gt
        self.transform = transform

        self.img_names = [os.path.join(root_dir_img, name) for name in os.listdir(root_dir_img)]
        self.gt_names = [os.path.join(root_dir_gt, name) for name in os.listdir(root_dir_gt)]

        self.img_names.sort()
        self.gt_names.sort()

    def __len__(self):
        return len(self.img_names)

    def __getitem__(self, idx):

        img = Image.open(self.img_names[idx])
        gt = Image.open(self.gt_names[idx])

        sample = {'image': img, 'mask': gt}

        if self.transform:
            sample = self.transform(sample)
            img = sample['image']
            gt = sample['mask']

        return img, gt




