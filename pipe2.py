import cv2
import matplotlib.patches as patches
import matplotlib.pyplot as plt
import numpy as np
import os
import pred
import torchvision
from PIL import Image
from skimage.feature import corner_peaks

"""
pipe2.py

- prende in input l'immagine di una pianta in vasetto o di una pianta croppata con minima bounding box possibile
- segmenta la pianta utilizzando Segnet
- crea una mappa di distanza per cercare di individuare i centroidi delle foglie
- segmenta le foglie singolarmente utilizzando una bounding box arbitraria intorno ai centroidi individuati
- ricostruisce l'immagine della pianta indicando ogni foglia con una label diversa
"""


def showoff(img, img_net, img_cmap, gt, local_max, cut):
    """
    - visualizza le immagini intermedie
    """

    x = []
    y = []
    blank = 25

    fig = plt.figure()
    ax1 = fig.add_subplot(231)
    ax2 = fig.add_subplot(232)
    ax3 = fig.add_subplot(233)
    ax4 = fig.add_subplot(234)
    ax5 = fig.add_subplot(235)
    ax6 = fig.add_subplot(236)

    ax1.imshow(img)
    ax1.text(img.size[0] / 2, img.size[1] + blank,
             'img',
             horizontalalignment='center',
             verticalalignment='center')

    ax2.imshow(img_net, 'gray')
    ax2.text(img.size[0] / 2, img.size[1] + blank,
             'segmentation',
             horizontalalignment='center',
             verticalalignment='center')

    ax3.imshow(img_net, 'gray')
    ax3.text(img.size[0] / 2, img.size[1] + blank,
             'centroids',
             horizontalalignment='center',
             verticalalignment='center')
    for i in range(len(local_max)):
        x.append(local_max[i][1])
        y.append(local_max[i][0])
        ax4.add_patch(patches.Rectangle((x[i] - cut, y[i] - cut),
                                        cut * 2,
                                        cut * 2,
                                        fill=False,
                                        edgecolor='r'))
    ax3.scatter(x, y, c='r')

    ax4.imshow(img)
    ax4.text(img.size[0] / 2, img.size[1] + blank,
             'img and crops',
             horizontalalignment='center',
             verticalalignment='center')

    ax5.imshow(gt)
    ax5.text(img.size[0] / 2, img.size[1] + blank,
             'gt',
             horizontalalignment='center',
             verticalalignment='center')

    ax6.imshow(img_cmap)
    ax6.text(img.size[0] / 2, img.size[1] + blank,
             'labels',
             horizontalalignment='center',
             verticalalignment='center')

    plt.show()


def leafextr(img, img_label, local_max, label):
    """
    - elabora le singole foglie
    """

    x = []
    y = []
    img_leaf = []
    coord = []
    w, h = img.size

    # ampiezza della bounding box intorno al centroide
    # un'ampiezza variabile in base all'intensità del massimo locale non migliora i risultati
    cut_val = np.mean([w / 5, h / 5])
    cut = int(round(cut_val))
    if cut < 5:
        cut = 20

    # correzione problemi di out of bounds
    for i in range(len(local_max)):
        x.append(local_max[i][1])
        y.append(local_max[i][0])

        xstart = x[i] - cut
        if xstart < 0:
            xstart = 0

        ystart = y[i] - cut
        if ystart < 0:
            ystart = 0

        xend = x[i] + cut
        if xend > img.size[0]:
            xend = img.size[0]

        yend = y[i] + cut
        if yend > img.size[1]:
            yend = img.size[1]

        img_leaf.append(img.crop((xstart, ystart, xend, yend)))
        coord.append([xstart, ystart, xend, yend])

    for i in range(len(img_leaf)):
        [xstart, ystart, xend, yend] = coord[i]

        # key indica il modello che si vuole usare per la segmentazione
        leaf_net = pred.cnnsegmentation(img_leaf[i], thresh=0.5, key='m15')

        # le immagini vengono resizate a 224x224 per ottenere risultati migliori
        # la rete è stata trainata e finetunata con quelle dimensioni
        # ma funziona anche senza effettuare il resize
        leaf_pil = Image.fromarray(leaf_net * 255)
        trans_resize = torchvision.transforms.Resize((yend - ystart, xend - xstart))
        leaf_net = trans_resize(leaf_pil)
        leaf_net = np.array(leaf_net)
        leaf_net[leaf_net > np.max(leaf_net) / 2] = 1
        leaf_net[leaf_net != 1] = 0

        """
        fig = plt.figure()
        ax1 = fig.add_subplot(121)
        ax2 = fig.add_subplot(122)
        ax1.imshow(img_leaf[i])
        ax2.imshow(leaf_net, 'gray')
        plt.ion()
        plt.pause(1)
        plt.show()
        plt.close()
        """

        # aggiunge l'ultima foglia elaborata alle altre già segmentate occupandosi delle label
        leaf_net = leaf_net - img_label[ystart:yend, xstart:xend]
        leaf_net[leaf_net < 0] = 0
        leaf_net[leaf_net == 1] = label

        img_label[ystart:yend, xstart:xend] = img_label[ystart:yend, xstart:xend] + leaf_net

        label += 1

    return img_label, label, cut


def pipeline(path_img, path_gt):
    img = Image.open(path_img)
    gt = Image.open(path_gt)

    w, h = img.size

    # inizializza sfondo nero dove verranno inserite le foglie segmentate
    img_label = np.zeros(np.shape(img)[:2])

    if img.mode == 'RGBA':
        data_conv = np.array(img)
        r, g, b, a = np.rollaxis(data_conv, axis=-1)
        data_conv = np.dstack([r, g, b])
        img = Image.fromarray(data_conv)

    # key indica il modello che si vuole usare per la segmentazione
    img_net = pred.cnnsegmentation(img, thresh=0.50, key='m14')

    # le immagini vengono resizate a 224x224 per ottenere risultati migliori
    # la rete è stata trainata e finetunata con quelle dimensioni
    # ma funziona anche senza effettuare il resize
    img_pil = Image.fromarray(img_net * 255)
    trans_resize = torchvision.transforms.Resize((h, w))
    img_net = trans_resize(img_pil)
    img_net = np.array(img_net)
    img_net[img_net > np.max(img_net) / 2] = 1
    img_net[img_net != 1] = 0

    # calcolo della mappa di distanza e dei massimi locali per individuare i centroidi
    img_cv2 = np.array(img_net * 255, dtype=np.uint8)
    dist_transform = cv2.distanceTransform(img_cv2, cv2.DIST_L2, 5)
    local_max = corner_peaks(dist_transform, min_distance=15, threshold_rel=0.4, indices=True)
    local_backup = []
    local_backup.extend(local_max)

    img_label, label, cut = leafextr(img, img_label, local_max, label=1)

    # visualizza i risultati intermedi di questa fase
    """
    cmap = plt.cm.get_cmap('jet')
    norm = plt.Normalize(vmin=img_label.min(), vmax=img_label.max())
    img_cmap = cmap(norm(img_label))
    showoff(img, img_net, img_cmap, gt, local_max, cut)
    """

    # EXTRA
    # viene ripetuta l'individuazione dei centroidi solo sulle parti non ancora elaborate
    # questo permette di individuare alcune delle foglie più piccole
    mask_cmap = np.array(img_label)
    mask_cmap[mask_cmap > 0] = 1
    mask_phtwo = img_net - mask_cmap
    mask_phtwo[mask_phtwo < 0] = 0
    kernel = np.ones((5, 5), np.uint8)
    mask_open = cv2.morphologyEx(mask_phtwo, cv2.MORPH_OPEN, kernel)
    mask_cv2 = np.array(mask_open * 255, dtype=np.uint8)

    dist_transform = cv2.distanceTransform(mask_cv2, cv2.DIST_L2, 5)
    local_max = corner_peaks(dist_transform, min_distance=15, threshold_rel=0.4, indices=True)
    local_backup.extend(local_max)

    img_label, label, cut = leafextr(img, img_label, local_max, label)

    # visualizza i risultati intermedi di questa fase
    """
    cmap = plt.cm.get_cmap('jet')
    norm = plt.Normalize(vmin=img_label.min(), vmax=img_label.max())
    img_cmap = cmap(norm(img_label))
    showoff(img, img_net, img_cmap, gt, local_max, cut)
    """

    # visualizza tutti i risultati intermedi
    """
    showoff(img, img_net, img_cmap, gt, local_backup, cut)
    """

    # EXTRA
    # viene effettuato un algoritmo di watershed (prendendo spunto dalla segmentazione con superpixel)
    # in questo modo si cerca di labellare tutta la pianta partendo dalle foglie individuate
    mask_water = np.array(img_label)
    mask_water[mask_water > 0] = 1

    mask_phthree = img_net - mask_water
    mask_phthree[mask_phthree < 0] = 0
    mask_phthree += 1
    mask_phthree[mask_phthree == 2] = 0

    img = img.convert('RGB')
    img = np.asarray(img, dtype=np.uint8) / 255
    mask = np.array(img_net, dtype=np.uint8)

    img_seg = cv2.bitwise_and(img, img, mask=mask)
    img_seg = np.array(img_seg * 255, dtype=np.uint8)
    markers = mask_phthree + img_label
    markers = np.array(markers, dtype=np.int32)
    # 0 = da classificare
    # 1 = sfondo
    # n = labels

    lab = 2
    if np.max(markers) == 1:
        for seed in local_backup:
            x = seed[1]
            y = seed[0]
            markers[y - 1:y + 1, x - 1:x + 1] = lab
            lab += 1

    markers = cv2.watershed(img_seg, markers)

    # rimuove i bordi che vengono creati da watershed
    h = markers.shape[0]
    w = markers.shape[1]
    markers[0, :] = 1
    markers[:, 0] = 1
    markers[:, w - 1] = 1
    markers[h - 1, :] = 1
    for i in range(h):
        for j in range(w):
            if markers[i, j] == -1:
                neighborhood = []
                neighborhood.append(markers[i - 1, j - 1])
                neighborhood.append(markers[i - 1, j])
                neighborhood.append(markers[i - 1, j + 1])
                neighborhood.append(markers[i, j + 1])
                neighborhood.append(markers[i + 1, j + 1])
                neighborhood.append(markers[i + 1, j])
                neighborhood.append(markers[i + 1, j - 1])
                neighborhood.append(markers[i, j - 1])

                markers[i, j] = max(set(neighborhood), key=neighborhood.count)

    markers[markers <= 1] = 0

    cmap = plt.cm.get_cmap('jet')
    norm = plt.Normalize(vmin=markers.min(), vmax=markers.max())
    img_cmap = cmap(norm(markers))

    """
    plt.imshow(img_cmap)
    plt.show()
    """

    return img_cmap


def main():
    path_img = './datasets/CVPPP2017_LSC_training/training/.../...'
    path_gt = './datasets/CVPPP2017_LSC_training/training/.../...'
    img_cmap = pipeline(path_img, path_gt)

    # codice per elaborare una folder e salvare i risultati
    """
    dir = './datasets/CVPPP2017_LSC_training/training/...'
    output_dir = './output'

    images = [os.path.join(dir, name) for name in os.listdir(dir) if
              os.path.isfile(os.path.join(dir, name)) & name.endswith('_rgb.png')]
    gts = [os.path.join(dir, name) for name in os.listdir(dir) if
           os.path.isfile(os.path.join(dir, name)) & name.endswith('_label.png')]

    images.sort()
    gts.sort()

    for i in range(len(images)):
        print(images[i], gts[i])

        file = pipeline(images[i], gts[i])
        filename = os.path.join(output_dir, os.path.basename(gts[i]))
        plt.imsave(filename, file)
    """


main()
