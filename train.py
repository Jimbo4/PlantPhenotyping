import argparse
import logger
import datetime
import os
import torch
import torch.backends.cudnn as cudnn
import torch.nn as nn
import torch.optim as optim
import transforms
from data import PlantDataset
from segnet import SegNet
from torch.autograd import Variable

"""
train.py

- effettua training e validation di SegNet
"""


def train(epoch):
    """
    - training SegNet
    """

    model.train()

    # update learning rate
    exp_lr_scheduler.step()

    total_loss = 0
    total_accuracy = 0

    # iteration over the batches
    for batch_idx, (img, gt) in enumerate(train_loader):

        if use_cuda:
            img = img.cuda(async=True)
            gt = gt.cuda(async=True)

        input = Variable(img)
        target = Variable(gt)

        # initialize gradients
        optimizer.zero_grad()

        # predictions
        output = model(input)

        # loss
        tb = target.size(0)
        th = target.size(2)
        tw = target.size(3)
        target_long = target.view(tb, th, tw).long()

        loss = cren_loss(output, target_long)
        loss.backward()
        optimizer.step()

        # accuracy
        # calcolata come media fra:
        # - la percentuale di pixel bianchi correttamente classificati come bianchi
        # - la percentuale di pixel neri correttamente classificati come neri
        output_pred = softmax(output)
        _, prediction = output_pred.max(dim=1)
        prediction = prediction.unsqueeze(1)

        mat_zero2zero = ((prediction == 0) * (target == 0)).int()
        mat_one2one = ((prediction == 1) * (target == 1)).int()

        prediction_back = mat_zero2zero.sum().float()
        target_back = target.numel() - target.sum()
        prediction_fore = mat_one2one.sum().float()
        target_fore = target.sum()

        acc_back = prediction_back / target_back
        acc_fore = prediction_fore / target_fore
        accuracy = (acc_back + acc_fore) / 2

        # print every 10 batch results
        if batch_idx % 10 == 0:
            print('batch: %8s | loss: %.3f | acc_back: %.3f | acc_fore: %.3f | acc: %.3f |'
                  % (str(batch_idx + 1) + '/' + str(len(train_loader)),
                     loss.data[0],
                     acc_back,
                     acc_fore,
                     accuracy),
                  datetime.datetime.now().strftime('%H:%M:%S'),
                  'training')

        total_loss += loss.data[0]
        total_accuracy += accuracy

    return total_loss / len(train_loader), total_accuracy / len(train_loader)


def val(epoch):
    """
    - validating SegNet
    """

    model.eval()

    total_loss = 0
    total_accuracy = 0

    # iteration over the batches
    for batch_idx, (img, gt) in enumerate(val_loader):

        if use_cuda:
            img = img.cuda(async=True)
            gt = gt.cuda(async=True)

        input = Variable(img, volatile=True)
        target = Variable(gt, volatile=True)

        # predictions
        output = model(input)

        # loss
        tb = target.size(0)
        th = target.size(2)
        tw = target.size(3)
        target_long = target.view(tb, th, tw).long()

        loss = cren_loss(output, target_long)

        # accuracy
        # calcolata come media fra:
        # - la percentuale di pixel bianchi correttamente classificati come bianchi
        # - la percentuale di pixel neri correttamente classificati come neri
        output_pred = softmax(output)
        _, prediction = output_pred.max(dim=1)
        prediction = prediction.unsqueeze(1)

        mat_zero2zero = ((prediction == 0) * (target == 0)).int()
        mat_one2one = ((prediction == 1) * (target == 1)).int()

        prediction_back = mat_zero2zero.sum().float()
        target_back = target.numel() - target.sum()
        prediction_fore = mat_one2one.sum().float()
        target_fore = target.sum()

        acc_back = prediction_back / target_back
        acc_fore = prediction_fore / target_fore
        accuracy = (acc_back + acc_fore) / 2

        # print every 10 batch results
        if batch_idx % 10 == 0:
            print('batch: %8s | loss: %.3f | acc_back: %.3f | acc_fore: %.3f | acc: %.3f |'
                  % (str(batch_idx + 1) + '/' + str(len(val_loader)),
                     loss.data[0],
                     acc_back,
                     acc_fore,
                     accuracy),
                  datetime.datetime.now().strftime('%H:%M:%S'),
                  'validating')

        total_loss += loss.data[0]
        total_accuracy += accuracy

    return total_loss / len(val_loader), total_accuracy / len(val_loader)


# training settings
parser = argparse.ArgumentParser(description='PyTorch SegNet')
parser.add_argument('--epochs', type=int, default=50, help='train epochs')
parser.add_argument('--lr', type=float, default=0.001, help='learning rate')
parser.add_argument('--momentum', type=float, default=0.9, help='SGD momentum')
args = parser.parse_args()

# cuda
use_cuda = torch.cuda.is_available()

input_nbr = 3
label_nbr = 2
img_size = 224

batch_size = 12
num_workers = 4

softmax = torch.nn.Softmax(dim=1)

if use_cuda:
    cren_loss = nn.CrossEntropyLoss().cuda()
else:
    cren_loss = nn.CrossEntropyLoss()

best_acc = 0
start_epoch = 0

# create SegNet model
model = SegNet(input_nbr, label_nbr)
model.load_from_filename('./model/m0e50plant_converted.pth')

# convert to cuda if needed
if use_cuda:
    model.cuda()
    cudnn.benchmark = True
else:
    model.float()

# finetuning
ftune_params = list(map(id, model.conv11d.parameters()))

base_params = filter(lambda p: id(p) not in ftune_params,
                     model.parameters())
new_params = filter(lambda p: id(p) in ftune_params,
                    model.parameters())

# optimizer
optimizer = torch.optim.SGD([{'params': base_params},
                             {'params': new_params, 'lr': args.lr}
                             ], lr=args.lr*0.1, momentum=args.momentum)

exp_lr_scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=15, gamma=0.1)

# define data
root_dir_img_train = './datasets/MyData/plant_train/img/'
root_dir_gt_train = './datasets/MyData/plant_train/gt/'

root_dir_img_val = './datasets/MyData/plant_val/img/'
root_dir_gt_val = './datasets/MyData/plant_val/gt/'

transform_train = transforms.Compose([
    transforms.ColorJitter(brightness=0.1, contrast=0.1, saturation=0.1, hue=0.1),
    transforms.RandomResizedCrop(img_size),
    transforms.RandomHorizontalFlip(),
    transforms.RandomVerticalFlip(),
    transforms.ToTensor()
])

transform_val = transforms.Compose([
    transforms.Resize((img_size, img_size)),
    transforms.ToTensor(),
])

print('Loading train dataset...')
train_plantdataset = PlantDataset(root_dir_img_train, root_dir_gt_train, transform_train)
train_loader = torch.utils.data.DataLoader(
    train_plantdataset,
    batch_size=batch_size,
    shuffle=True,
    num_workers=num_workers,
    pin_memory=True
)

print('Loading validation dataset...')
val_plantdataset = PlantDataset(root_dir_img_val, root_dir_gt_val, transform_val)
val_loader = torch.utils.data.DataLoader(
    val_plantdataset,
    batch_size=batch_size,
    shuffle=False,
    num_workers=num_workers,
    pin_memory=True
)

# set the logger
log = logger.Logger('./logs')

for epoch in range(start_epoch, start_epoch + args.epochs):
    print('EPOCH:', str(epoch+1), '\n')

    # training
    train_loss, train_acc = train(epoch)
    print('\nepoch: %8s | loss: %.3f | acc: %.3f |'
          % (str(epoch + 1) + '/' + str(start_epoch + args.epochs),
             train_loss,
             train_acc),
          datetime.datetime.now().strftime('%H:%M:%S'),
          'training',
          '\n')

    # validation
    val_loss, val_acc = val(epoch)
    print('\nepoch: %8s | loss: %.3f | acc: %.3f |'
          % (str(epoch + 1) + '/' + str(start_epoch + args.epochs),
             val_loss,
             val_acc),
          datetime.datetime.now().strftime('%H:%M:%S'),
          'validating',
          '\n')

    # saving best model
    if val_acc.cpu().data.numpy() > best_acc:
        print('Saving best model state dict...', '\n')

        if best_acc > 0:
            os.remove(checkpoint)
        best_acc = val_acc.cpu().data.numpy()
        checkpoint = './model/model_epoch' + str(epoch + 1) + '_checkpoint.pth'
        torch.save(model.state_dict(), checkpoint)

    # Tensorboard logging
    info = {'train-loss': train_loss,
            'train-acc': train_acc.cpu().data.numpy(),
            'val-loss': val_loss,
            'val-accuracy': val_acc.cpu().data.numpy()}

    for tag, value in info.items():
        log.scalar_summary(tag, value, epoch + 1)

# LOGGER:
# python3 -m tensorboard.main --logdir='/.../logs' --port='6006'
# http://localhost:6006/

